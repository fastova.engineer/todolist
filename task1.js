const btnAddEl = document.querySelector('.js-btn-add');
const inputEl = document.querySelector('.js-input');
const btnCancel = document.querySelector('.js-cancel');

restoreAppState();

btnAddEl.addEventListener('click', () => {
    //Check If Shadow exists
    if (document.querySelector('li.toggle-shadow')) {
        /// yes: rewrite text in li
        document.querySelector('li.toggle-shadow').querySelector('span').textContent = inputEl.value;

        let text = inputEl.value;
        let li = document.querySelector('li.toggle-shadow')
        let ul = li.parentNode
        let index = Array.prototype.slice.call(ul.children).indexOf(li);
        lsEditItem(index, text);

    } else {
        // no:
        const name = inputEl.value || 'Empty task';
        const item = {
            name
        };
        createLiEl(item);
        lSAddItem(item);
    }

    clearAppState();
});

btnCancel.addEventListener('click', clearAppState);

document.querySelector('.js-group').addEventListener('click', event => {

    // 2. Checkbox.
    if (event.target.classList.contains('js-checkbox')) {
        const parentNode = event.target.parentNode;
        let isChecked = parentNode.querySelector('.js-item-value').classList.toggle('done');

        const li = event.target.parentNode.parentNode;
        const ul = event.target.parentNode.parentNode.parentNode;
        let index = Array.prototype.slice.call(ul.children).indexOf(li);
        changeChecked(index, isChecked)
    }

    // 3. Delete item;
    if (event.target.classList.contains('js-remove')) {
        const li = event.target.parentNode;
        const ul = li.parentNode;
        let key = Array.prototype.slice.call(ul.children).indexOf(li);
        lsRemoveItem(key);
        li.remove();
    }
    // 4. Shadow DOM for editing
    if (event.target.classList.contains('list-group-item')) {
        const previousShadow = document.querySelector('li.toggle-shadow');
        if (previousShadow) {
            previousShadow.classList.remove('toggle-shadow');
        }
        event.target.classList.add('toggle-shadow');

        let mainInput = document.querySelector('.js-input');
        mainInput.value = event.target.querySelector('.js-item-value').textContent;


        if (btnAddEl.innerText === "Add") {
            btnAddEl.innerText = "Update";
        }

        if (btnCancel.classList.contains('d-none')) {
            btnCancel.classList.remove('d-none');
        }


    }
});

function restoreAppState() {
    // Get items from LocalStore
    let items = lsGetAll();
    // Display it in DOM
    items.forEach(createLiEl)
}

function clearAppState() {

    btnCancel.classList.add('d-none');
    if (btnAddEl.innerText === "Update") {
        btnAddEl.innerText = "Add";
    }
    inputEl.value = '';

    if (document.querySelector('li.toggle-shadow')) {
        document.querySelector('li.toggle-shadow').classList.remove('toggle-shadow');
    }

}

function createLiEl(newElem) {
    const liEl = document.createElement('li');
    liEl.classList.add('list-group-item');
    liEl.classList.add('js-item');

    const formGroupEl = document.createElement('div');
    formGroupEl.classList.add('form-group');

    // Create checkbox
    const checkboxEl = document.createElement('input');
    checkboxEl.classList.add('form-check-input');
    checkboxEl.classList.add('js-checkbox');
    checkboxEl.setAttribute('type', 'checkbox');

    if (newElem.isChecked !== undefined && newElem.isChecked === true) {
        checkboxEl.setAttribute('checked', 'true')
    }
    //end create checkbox


    const spanEl = document.createElement('span');
    spanEl.classList.add('js-item-value');
    spanEl.textContent = newElem.name;

    const removeEl = document.createElement('div');
    removeEl.classList.add('remove', 'js-remove');
    removeEl.textContent = 'X';

    formGroupEl.append(checkboxEl);
    formGroupEl.append(spanEl);

    liEl.append(formGroupEl);
    liEl.append(removeEl);

    document.querySelector('.js-group').append(liEl);

    return liEl;

}

function lsRemoveItem(key) {
    let itemsInLS = lsGetAll();
    itemsInLS.splice(key, 1);
    lsSetAll(itemsInLS);
}

function lSAddItem(element) {
    let items = lsGetAll();
    items.push(element);
    lsSetAll(items)
}

function lsEditItem(index, newName) {
    let items = lsGetAll();
    items[index].name = newName;
    lsSetAll(items)
}

function lsGetAll() {
    if (window.localStorage['tasksList'] === undefined) {
        lsSetAll([]);
        return [];
    }

    return JSON.parse(window.localStorage['tasksList']);
}

function lsSetAll(items) {
    window.localStorage['tasksList'] = JSON.stringify(items);
}

function changeChecked(index, state) {
    let items = lsGetAll();
    items[index].isChecked = state;
    lsSetAll(items)
}



















































